$(document).ready(function () {
    var deviceAgent = navigator.userAgent.toLowerCase();
    var rpItem = '.itt_rp-item';
    var btnAmazon = '.itt_btn-amazon';
    var btnShop = '.itt_btn-shop';
    var shortDesc = '.itt_short-desc';
    var navItem = '.itt_nav-item';

    $(navItem).hover(function () {
        $('.itt_stick-wrap', this).show();
    }, function () {
        $('.itt_stick-wrap', this).hide();
    });

    // Owl carousel
    var owl = $("#owl");

    owl.owlCarousel({
        items : 4,
        itemsDesktop : [1000,5],
        itemsDesktopSmall : [900,3],
        itemsTablet: [600,1],
        itemsMobile : true,
        loop:true,
        autoPlay: true,
        autoPlayTimeout: 1000
    });


    if (!deviceAgent.match(/mobile/)) {
        $('.itt_social-link').hover(function () {
            $(this).addClass('fadeIn');
        }, function () {
            $(this).removeClass('fadeIn');
        });

        $(rpItem).hover(function () {
            $(btnShop, this).stop();
            $(btnShop, this).animate({'left': '0'});

            $(shortDesc, this).stop();
            $(shortDesc, this).animate({'top': '0'});
        }, function () {
            $(btnShop, this).stop();
            $(btnShop, this).animate({'left': '-50%'});

            $(shortDesc, this).stop();
            $(shortDesc, this).animate({'top': '-100px'});
        });

        $(rpItem).hover(function () {
            $(btnAmazon, this).stop();
            $(btnAmazon, this).animate({'right': '0'});
        }, function () {
            $(btnAmazon, this).stop();
            $(btnAmazon, this).animate({'right': '-50%'});
        });

        $('.itt_rp-price').hover(function () {
            $(this).addClass('pulse');
        }, function () {
            $(this).removeClass('pulse');
        });
    } else {
        $('.itt_btns-affiliate .itt_btn-shop').addClass('itt_btn-shop-mobile');
        $('.itt_btns-affiliate .itt_btn-amazon').addClass('itt_btn-amazon-mobile');
    }

    $(window).scroll(function () {
        var topPad = $(window).scrollTop();

        if (topPad > 200) {
            $('.itt_arrow-top').removeClass('fadeOut');
            $('.itt_arrow-top').css('bottom', '30px');
            $('.itt_arrow-top').addClass('fadeIn');

            $('.itt_header-sticky').css('transition', 'all 0.5s ease-in-out');
            $('.itt_header-sticky').css('background-color', 'rgba(7, 23, 42, 0.8)');
            $('.itt_logo img').css('transition', 'all 0.5s ease-in-out');
        } else {
            $('.itt_arrow-top').removeClass('fadeIn');
            $('.itt_arrow-top').addClass('fadeOut');
            $('.itt_arrow-top').css('bottom', '-50px');

            $('.itt_header-sticky').css('background-color', 'rgba(7, 23, 42, 1)');
        }
    });

    $('a.itt_arrow-top').on('click', function () {
        $('html, body').stop().animate({
            scrollTop: 0
        }, 1000);
    });

    $('#itt_sandwich').on('click',function(){
        $('.itt_sw-topper').toggleClass('sandwich-animate-top');
        $('.itt_sw-bottom').toggleClass('sandwich-animate-bottom');
        $('.itt_navigation').slideToggle();
    });

    $('.itt_nav-list a').on('click', function () {
        $('.itt_nav-list a').removeClass('active');
        $(this).addClass('active');
    });

    if ($(window).width() >= 768) {
        $('#itt_search-button').on('click', function () {
            $('.itt_search-desktop').toggleClass('show');
        });
    }

    $('.itt_tab-panel').first().show();

    $('.itt_tabs a').on('click', function (e) {
        e.preventDefault();

        var href = $(this).attr('href');
        $('.itt_tab-panel').hide();
        $(href).show();
        $('.itt_tab').removeClass('active');
        $(this).parent().addClass('active');
    });

    $('#container').masonry({
        itemSelector: '.blog-item'
    });
});